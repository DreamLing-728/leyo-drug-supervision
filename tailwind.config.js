module.exports = {
  content: [
    './public/index.html',
    './src/**/*.{vue,js,ts,jsx,tsx,scss,less}',
    './src/*.{vue,js,ts,jsx,tsx,scss,less}',
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {},
    spacing: {
      1: '10px',
      2: '20px',
      3: '40px',
      md: '14px',
      sm: '12px',
      lg: '16px',
    },
    fontSize: {
      md: '14px',
      sm: '12px',
      lg: '16px',
    },
  },
  plugins: [],
};
