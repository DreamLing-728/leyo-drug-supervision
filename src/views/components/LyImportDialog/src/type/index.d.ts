/* eslint-disable @typescript-eslint/no-explicit-any */
import { Obj } from '@src/types/global';
import { UploadRawFile } from 'element-plus';
/**
 * 上传解析请求方法
 */
export interface UploadApi {
  (params: UploadApiParams): UploadApiRes;
}

/**
 * 上传解析请求方法参数
 */
export interface UploadApiParams extends Obj {
  file: UploadRawFile;
  [propsName: string]: any;
}

/**
 * 上传解析请求方法返回值
 */
export interface UploadApiRes {
  validList?: Obj[];
  invalidList?: Obj[];
  [propsName: string]: any;
}

/**
 * 过滤方法
 */
export interface ImportFilterFun {
  (validList: Obj[], invalidList?: Obj[]): {
    validList: Obj[];
    invalidList?: Obj[];
  };
}
