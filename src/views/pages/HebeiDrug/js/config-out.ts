const CONFIG_OUT = [
  {
    label: '中心品种编码',
    prop: 'iitemCode',
    width: '100px',
  },
  {
    label: '生产批号',
    prop: 'ypph',
    width: '100px',
  },
  {
    label: '有效期限',
    prop: 'vdate',
    width: '100px',
  },
  {
    label: '包装规格',
    prop: 'specification',
    width: '100px',
  },
  {
    label: '计量单位',
    prop: 'unit',
    width: '100px',
  },
  {
    label: '出库（销售）数量',
    prop: 'num',
    width: '100px',
  },
  {
    label: '出库时间',
    prop: 'jzDate',
    width: '100px',
  },
  {
    label: '出库单号',
    prop: 'fph',
    width: '100px',
  },
];
export { CONFIG_OUT };
