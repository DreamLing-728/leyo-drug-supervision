import { Ref, ref } from 'vue';
import { PARAMS_CODE_TO_VARIETIES_DATA, CODE_TO_VARIETIES_ITEM } from './type';
import { getCodeToVarietiesList } from '@src/controller/api/HebeiDrug/codeToVarieties';
import { DRUG_TYPE } from '@src/types/common';

/*
  查询
*/
function searchController() {
  const tableLoading = ref(false);
  const dataList: Ref<CODE_TO_VARIETIES_ITEM[]> = ref([]);
  const totalCount = ref(0);
  const fetchCodeToVarietiesDataList = async (
    params: PARAMS_CODE_TO_VARIETIES_DATA,
  ) => {
    try {
      tableLoading.value = true;
      const data = await getCodeToVarietiesList(params);
      data.records &&
        data.records.forEach((item: CODE_TO_VARIETIES_ITEM) => {
          item.itemTypeDesc = DRUG_TYPE[Number(item.itemType)];
        });
      dataList.value = data.records || [];
      totalCount.value = data.total as number;
    } catch (error) {
      console.error(error);
      tableLoading.value = false;
    } finally {
      tableLoading.value = false;
    }
  };
  return {
    dataList,
    totalCount,
    tableLoading,
    fetchCodeToVarietiesDataList,
  };
}

export { searchController };
