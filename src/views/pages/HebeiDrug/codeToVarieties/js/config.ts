const CODE_TO_VARIETIES_CONFIG = [
  {
    label: '通用名',
    prop: 'commonName',
    width: '100px',
  },
  {
    label: 'ERP品种编码',
    prop: 'goodsCode',
    width: '100px',
  },
  {
    label: '药监平台品种编码',
    prop: 'itemCode',
    width: '100px',
  },
  {
    label: '药监平台品种类型',
    prop: 'itemTypeDesc',
    width: '100px',
  },
  {
    label: '规格',
    prop: 'specification',
    width: '100px',
  },
  {
    label: '创建时间',
    prop: 'ctime',
    width: '100px',
  },
  {
    label: '修改时间',
    prop: 'mtime',
    width: '100px',
  },
];
export { CODE_TO_VARIETIES_CONFIG };
