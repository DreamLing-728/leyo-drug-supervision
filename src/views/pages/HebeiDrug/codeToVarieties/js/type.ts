import { PaginationParams } from '@src/types/common';

export interface PARAMS_CODE_TO_VARIETIES_DATA extends PaginationParams {
  commonName?: string; // 通用名称
  goodsCode?: string; // ERP品种编码
  itemCode?: string; // 药监平台品种编码
  itemType?: string; // 药监平台品种类型（0：药品制剂 1：中药饮片）
}

export type CODE_TO_VARIETIES_ITEM = {
  goodsCode?: string | number;
  itemCode?: string;
  itemType?: string;
  itemTypeDesc?: string;
  ctime?: string;
  mtime?: string;
  specification?: string;
};

export type RESPONSE_CODE_TO_VARIETIES_DATA = {
  total?: number;
  records?: CODE_TO_VARIETIES_ITEM[];
};
