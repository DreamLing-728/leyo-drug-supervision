import { Ref, ref } from 'vue';
import { RESPONSE_BILL_TYPE_IN } from '@src/types/common';
import { PARAMS_PURCHASE_DATA, PURCHASE_ITEM } from './type';
import {
  getPurchaseInDataList,
  exportPurchaseInData,
} from '@src/controller/api/HebeiDrug/purchaseData';

/*
  查询
*/
function searchController() {
  const tableLoading = ref(false);
  const dataList: Ref<PURCHASE_ITEM[]> = ref([]);
  const totalCount = ref(0);
  const fetchPurchaseInDataList = async (params: PARAMS_PURCHASE_DATA) => {
    try {
      tableLoading.value = true;
      const data = await getPurchaseInDataList(params);
      data.records &&
        data.records.forEach((item: PURCHASE_ITEM) => {
          item.billTypeDesc = RESPONSE_BILL_TYPE_IN[item.billType as number];
        });
      dataList.value = data.records || [];
      totalCount.value = data.total as number;
    } catch (error) {
      console.error(error);
      tableLoading.value = false;
    } finally {
      tableLoading.value = false;
    }
  };
  return {
    dataList,
    totalCount,
    tableLoading,
    fetchPurchaseInDataList,
  };
}

/*
  导出
*/
function exportController() {
  const downLoading = ref(false);
  const downPurchaseInData = async (
    params: PARAMS_PURCHASE_DATA,
    fileName: string,
  ) => {
    try {
      downLoading.value = true;
      await exportPurchaseInData(params, fileName);
    } catch (error) {
      console.error(error);
      downLoading.value = false;
    } finally {
      downLoading.value = false;
    }
  };
  return {
    downLoading,
    downPurchaseInData,
  };
}
export { searchController, exportController };
