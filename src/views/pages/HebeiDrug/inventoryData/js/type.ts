import { PaginationParams } from '@src/types/common';

export interface PARAMS_INVENTORY_DATA extends PaginationParams {
  batchCode?: string; // 批号
  goodsCode?: string; // ERP的品种编码
  itemCode?: string; // 平台中心的品种编号
}

export type INVENTORY_ITEM = {
  barcode?: string | number;
  iitemCode?: string;
  jzDate?: string;
  num?: string;
  specification?: string;
  unit?: string;
  ypph?: string;
};

export type RESPONSE_INVENTORY_DATA = {
  total?: number;
  records?: INVENTORY_ITEM[];
};
