import { Ref, ref } from 'vue';
import { PARAMS_INVENTORY_DATA, INVENTORY_ITEM } from './type';
import {
  getInventoryDataList,
  exportInventoryData,
} from '@src/controller/api/HebeiDrug/inventoryData';

/*
  查询
*/
function searchController() {
  const tableLoading = ref(false);
  const dataList: Ref<INVENTORY_ITEM[]> = ref([]);
  const totalCount = ref(0);
  const fetchPurchaseInDataList = async (params: PARAMS_INVENTORY_DATA) => {
    try {
      tableLoading.value = true;
      const data = await getInventoryDataList(params);
      dataList.value = data.records || [];
      totalCount.value = data.total as number;
    } catch (error) {
      console.error(error);
      tableLoading.value = false;
    } finally {
      tableLoading.value = false;
    }
  };
  return {
    dataList,
    totalCount,
    tableLoading,
    fetchPurchaseInDataList,
  };
}

/*
  导出
*/
function exportController() {
  const downLoading = ref(false);
  const downPurchaseInData = async (
    params: PARAMS_INVENTORY_DATA,
    fileName: string,
  ) => {
    try {
      downLoading.value = true;
      await exportInventoryData(params, fileName);
    } catch (error) {
      console.error(error);
      downLoading.value = false;
    } finally {
      downLoading.value = false;
    }
  };
  return {
    downLoading,
    downPurchaseInData,
  };
}
export { searchController, exportController };
