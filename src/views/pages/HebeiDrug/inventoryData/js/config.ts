const INVENTORY_DATA_TABLE_CONFIG = [
  {
    label: '中心品种编码',
    prop: 'iitemCode',
    width: '100px',
  },
  {
    label: '药品条形码',
    prop: 'barcode',
    width: '100px',
  },
  {
    label: '生产批号',
    prop: 'ypph',
    width: '100px',
  },
  {
    label: '有效期',
    prop: 'vdate',
    width: '100px',
  },
  {
    label: '包装规格',
    prop: 'specification',
    width: '100px',
  },
  {
    label: '计量单位',
    prop: 'unit',
    width: '100px',
  },
  {
    label: '库存数量',
    prop: 'num',
    width: '100px',
  },
  {
    label: '库存时间',
    prop: 'jzDate',
    width: '100px',
  },
];
export { INVENTORY_DATA_TABLE_CONFIG };
