import { PaginationParams } from '@src/types/common';

export interface PARAMS_SALES_DATA extends PaginationParams {
  billCode?: string; // 采购入库/采购退回单号
  billType?: string; // 入库类型
  clientName?: string; // 客户名称
  clientId?: string; // 客户id
  startDate?: string; // 入库/退回开始时间,yyyyMMdd
  endDate?: string; // 入库/退回结束时间,yyyyMMdd
  goodsCode?: string; // ERP的品种编码
  itemCode?: string; // 平台中心的品种编号
}

export type SALES_ITEM = {
  billType?: string | number;
  billTypeDesc?: string;
  fph?: string | number;
  jzDate?: string;
  merName?: string;
  meraCode?: string | number;
  num?: string | number;
  specification?: string;
  unit?: string;
  ypph?: string;
};

export type RESPONSE_SALES_DATA = {
  total?: number;
  records?: SALES_ITEM[];
};
