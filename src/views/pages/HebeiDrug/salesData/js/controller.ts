import { Ref, ref } from 'vue';
import { RESPONSE_BILL_TYPE_OUT } from '@src/types/common';
import { PARAMS_SALES_DATA, SALES_ITEM } from './type';
import {
  getSalesDataList,
  exportSalesData,
} from '@src/controller/api/HebeiDrug/salesData';

/*
  查询
*/
function searchController() {
  const tableLoading = ref(false);
  const dataList: Ref<SALES_ITEM[]> = ref([]);
  const totalCount = ref(0);
  const fetchPurchaseInDataList = async (params: PARAMS_SALES_DATA) => {
    try {
      tableLoading.value = true;
      const data = await getSalesDataList(params);
      data.records &&
        data.records.forEach((item: SALES_ITEM) => {
          item.billTypeDesc = RESPONSE_BILL_TYPE_OUT[item.billType as number];
        });
      dataList.value = data.records || [];
      totalCount.value = data.total as number;
    } catch (error) {
      console.error(error);
      tableLoading.value = false;
    } finally {
      tableLoading.value = false;
    }
  };
  return {
    dataList,
    totalCount,
    tableLoading,
    fetchPurchaseInDataList,
  };
}

/*
  导出
*/
function exportController() {
  const downLoading = ref(false);
  const downPurchaseInData = async (
    params: PARAMS_SALES_DATA,
    fileName: string,
  ) => {
    try {
      downLoading.value = true;
      await exportSalesData(params, fileName);
    } catch (error) {
      console.error(error);
      downLoading.value = false;
    } finally {
      downLoading.value = false;
    }
  };
  return {
    downLoading,
    downPurchaseInData,
  };
}
export { searchController, exportController };
