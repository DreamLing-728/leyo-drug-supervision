const PURCHASE_DATA_TABLE_CONFIG = [
  {
    label: '中心品种编码',
    prop: 'iitemCode',
    width: '100px',
  },
  {
    label: '生产批号',
    prop: 'ypph',
    width: '100px',
  },
  {
    label: '有效期限',
    prop: 'vdate',
    width: '100px',
  },
  {
    label: '包装规格',
    prop: 'specification',
    width: '100px',
  },
  {
    label: '计量单位',
    prop: 'unit',
    width: '100px',
  },
  {
    label: '销售数量',
    prop: 'num',
    width: '100px',
  },
  {
    label: '出库时间',
    prop: 'jzDate',
    width: '100px',
  },

  {
    label: '出库单号',
    prop: 'fph',
    width: '100px',
  },
  {
    label: '统一社会信用代码',
    prop: 'imerCode',
    width: '100px',
  },
  {
    label: '客户名称',
    prop: 'merName',
    width: '100px',
  },
  {
    label: '客户区域编码',
    prop: 'meraCode',
    width: '100px',
  },
  {
    label: '出库类型',
    prop: 'billTypeDesc',
    width: '100px',
  },
];
export { PURCHASE_DATA_TABLE_CONFIG };
