import { cloneDeep } from 'lodash';

export interface MENU_INFO {
  icon?: string;
  activeIcon?: string;
  label: string;
  path: string;
  id: number;
  module: string;
  children?: MENU_INFO[];
}
export const staticMenu: MENU_INFO[] = [
  {
    label: '首页',
    module: 'index',
    path: '/',
    id: 0,
  },
  {
    label: '河北药监',
    module: 'HeBeiDrugSupervision',
    // icon: 'i-edit',
    // activeIcon: '#icon-todo-list-active',
    path: '/hebei-drug-supervision',
    id: 1,
    children: [
      {
        label: '采购数据',
        module: 'purchaseData',
        path: '/hebei-drug-supervision/purchase-data',
        id: 2,
      },
      {
        label: '库存数据',
        module: 'inventoryData',
        path: '/hebei-drug-supervision/inventory-data',
        id: 3,
      },
      {
        label: '生产入库',
        module: 'productionIn',
        path: '/hebei-drug-supervision/production-in',
        id: 4,
      },
      {
        label: '报损出库',
        module: 'damageOut',
        path: '/hebei-drug-supervision/damage-out',
        id: 5,
      },
      {
        label: '编码与品种关系',
        module: 'codeToVarieties',
        path: '/hebei-drug-supervision/code-to-varieties',
        id: 6,
      },
      {
        label: '销售数据',
        module: 'saleData',
        path: '/hebei-drug-supervision/sale-data',
        id: 7,
      },
      {
        label: '盘盈入库',
        module: 'profitIn',
        path: '/hebei-drug-supervision/profit-in',
        id: 8,
      },
      {
        label: '盘亏出库',
        module: 'lossOut',
        path: '/hebei-drug-supervision/loss-out',
        id: 9,
      },
    ],
  },
];

export const getFilteredMenu = (menuTree: MENU_INFO[], modules: string[]) => {
  const result = [];
  for (const itemAll of menuTree) {
    if (modules.includes(itemAll.module)) {
      // 匹配上一级目录
      const outer = cloneDeep(itemAll);
      if (outer.children) outer.children = [];
      if (itemAll.children && itemAll.children.length > 0) {
        // 二级目录
        for (const inner of itemAll.children) {
          if (modules.includes(inner.module)) {
            outer.children?.push(inner);
          }
        }
      }
      result.push(outer);
    }
  }
  return result;
};
