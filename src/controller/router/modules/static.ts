import { IcarusRoute } from '../types';

const staticRoutes: IcarusRoute[] = [
  {
    path: '/hebei-drug-supervision',
    name: 'HeBeiDrugSupervision',
    meta: {
      label: '',
    },
    component: () => import('@views/pages/HebeiDrug/index.vue'),
    children: [
      {
        path: '/hebei-drug-supervision/purchase-data',
        name: '采购数据',
        meta: {
          label: '采购数据',
        },
        component: () =>
          import('@views/pages/HebeiDrug/purchaseData/purchaseData.vue'),
      },
      {
        path: '/hebei-drug-supervision/inventory-data',
        name: '库存数据',
        meta: {
          label: '库存数据',
        },
        component: () =>
          import('@views/pages/HebeiDrug/inventoryData/inventoryData.vue'),
      },
      {
        path: '/hebei-drug-supervision/production-in',
        name: '生产入库',
        meta: {
          label: '生产入库',
        },
        component: () =>
          import('@views/pages/HebeiDrug/productionIn/productionIn.vue'),
      },
      {
        path: '/hebei-drug-supervision/damage-out',
        name: '报损出库',
        meta: {
          label: '报损出库',
        },
        component: () =>
          import('@views/pages/HebeiDrug/damageOut/damageOut.vue'),
      },
      {
        path: '/hebei-drug-supervision/code-to-varieties',
        name: '编码与品种关系',
        meta: {
          label: '编码与品种关系',
        },
        component: () =>
          import('@views/pages/HebeiDrug/codeToVarieties/codeToVarieties.vue'),
      },
      {
        path: '/hebei-drug-supervision/sale-data',
        name: '销售数据',
        meta: {
          label: '销售数据',
        },
        component: () =>
          import('@views/pages/HebeiDrug/salesData/salesData.vue'),
      },
      {
        path: '/hebei-drug-supervision/profit-in',
        name: '盘盈入库',
        meta: {
          label: '盘盈入库',
        },
        component: () => import('@views/pages/HebeiDrug/profitIn/profitIn.vue'),
      },
      {
        path: '/hebei-drug-supervision/loss-out',
        name: '盘亏出库',
        meta: {
          label: '盘亏出库',
        },
        component: () => import('@views/pages/HebeiDrug/lossOut/lossOut.vue'),
      },
    ],
  },
];

export default staticRoutes;
