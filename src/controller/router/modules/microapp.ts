import { IcarusRoute } from '../types';

const microappRoute: IcarusRoute[] = [
  {
    path: '/microapp-route/:page*',
    name: 'microapp-route',
    meta: {
      label: '微应用页面',
    },
    component: () => import('@views/pages/system/microapp/index.vue'),
  },
];

export default microappRoute;
