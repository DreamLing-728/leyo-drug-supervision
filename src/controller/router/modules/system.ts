import { IcarusRoute } from '../types';

const systemRoutes: IcarusRoute[] = [
  {
    path: '/',
    name: '首页',
    component: () => import('@views/pages/index/index.vue'),
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      hideSiderBar: true,
      hideHeaderBar: true,
    },
    component: () => import('@views/pages/system/login/login.vue'),
  },
];
export default systemRoutes;
