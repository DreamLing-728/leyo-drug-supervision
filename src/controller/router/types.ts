import { RouteRecordRaw } from 'vue-router';

export interface IcarusRouteMeta {
  hideSiderBar?: boolean;
  hideHeaderBar?: boolean;
  [prop: string]: any;
}

export type IcarusRoute = RouteRecordRaw & { meta?: IcarusRouteMeta };
