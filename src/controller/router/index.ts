import { createRouter, createWebHistory } from 'vue-router';
import { IcarusRoute } from './types';
import { NavInstance } from '@src/types/layout';
import { appStoreWithoutSetup } from '@store/app';
import staticRoutes from './modules/static';
import microappRoutes from './modules/microapp';
import system from './modules/system';
import { getToken } from '@src/utils/auth';
import { gotoLogin } from '@src/utils';

const routes: Array<IcarusRoute> = [
  ...system,
  ...staticRoutes,
  ...microappRoutes,
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.afterEach(async (from, to) => {
  // clearToken();
  // 无token跳转登录页
  const token = getToken();
  if (!token && to.fullPath !== '/login') {
    gotoLogin();
  }
});

router.afterEach((to) => {
  const app = appStoreWithoutSetup();
  const path = to.path;
  const navigatorItem: NavInstance = {
    label: to.name,
    path,
    active: true,
    closable: path !== '/',
    query: to.query,
    params: to.params,
  };
  app.addNavigator(navigatorItem);
  app.setCurrentPath(path);
});

export default router;
