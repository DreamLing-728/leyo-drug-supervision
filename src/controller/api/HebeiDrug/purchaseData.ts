import { post, download } from '@src/utils/request';
import {
  PARAMS_PURCHASE_DATA,
  RESPONSE_PURCHASE_DATA,
} from '@views/pages/HebeiDrug/purchaseData/js/type';
/**
 * 陈建容 - 采购数据列表
 * @param params
 * @returns
 */
export function getPurchaseInDataList(
  params: PARAMS_PURCHASE_DATA,
): Promise<RESPONSE_PURCHASE_DATA> {
  return post({
    url: '/hebei/drugAdministration/purchaseInDataList',
    params,
  });
}

/**
 * 陈建容 - 导出采购数据列表
 * @param params
 * @returns
 */
export function exportPurchaseInData(
  params: PARAMS_PURCHASE_DATA,
  fileName: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  return download({
    method: 'post',
    url: '/hebei/drugAdministration/exportPurchaseInData',
    params,
    fileConfig: {
      name: fileName,
      type: 'application/zip',
    },
  });
}
