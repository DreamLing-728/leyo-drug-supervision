import { post, upload } from '@src/utils/request';
import {
  PARAMS_CODE_TO_VARIETIES_DATA,
  RESPONSE_CODE_TO_VARIETIES_DATA,
} from '@views/pages/HebeiDrug/codeToVarieties/js/type';
/**
 * 陈建容 - 品种与编码关系数据列表
 * @param params
 * @returns
 */
export function getCodeToVarietiesList(
  params: PARAMS_CODE_TO_VARIETIES_DATA,
): Promise<RESPONSE_CODE_TO_VARIETIES_DATA> {
  return post({
    url: '/hebei/drugAdministration/drugGoodsList',
    params,
  });
}

/**
 * 陈建容 - 导入品种与编码关系数据
 * @param params
 * @returns
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function importCodeToVarieties(params: any): Promise<any> {
  return upload({
    url: '/hebei/drugAdministration/importAdministrationGoods',
    params,
  });
}

/**
 * 陈建容 - 删除关系数据
 * @param params
 * @returns
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function deleteAdministrationGoods(params: Array<any>): Promise<any> {
  return post({
    url: '/hebei/drugAdministration/deleteAdministrationGoods',
    params,
  });
}
