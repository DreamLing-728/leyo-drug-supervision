import { post, download } from '@src/utils/request';
import {
  PARAMS_LOSS_OUT_DATA,
  RESPONSE_LOSS_OUT_DATA,
} from '@views/pages/HebeiDrug/lossOut/js/type';
/**
 * 陈建容 - 采购数据列表
 * @param params
 * @returns
 */
export function getLossOutDataList(
  params: PARAMS_LOSS_OUT_DATA,
): Promise<RESPONSE_LOSS_OUT_DATA> {
  return post({
    url: '/hebei/drugAdministration/invntryExecOutDataList',
    params,
  });
}

/**
 * 陈建容 - 导出采购数据列表
 * @param params
 * @returns
 */
export function exportLossOutData(
  params: PARAMS_LOSS_OUT_DATA,
  fileName: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  return download({
    method: 'post',
    url: '/hebei/drugAdministration/exportInvntryExecOutData',
    params,
    fileConfig: {
      name: fileName,
      type: 'application/zip',
    },
  });
}
