import { post, download } from '@src/utils/request';
import {
  PARAMS_PRODUCTION_IN_DATA,
  RESPONSE_PRODUCTION_IN__DATA,
} from '@views/pages/HebeiDrug/productionIn/js/type';
/**
 * 陈建容 - 生产入库数据列表
 * @param params
 * @returns
 */
export function getProductInDataList(
  params: PARAMS_PRODUCTION_IN_DATA,
): Promise<RESPONSE_PRODUCTION_IN__DATA> {
  return post({
    url: '/hebei/drugAdministration/alExecInDataList',
    params,
  });
}

/**
 * 陈建容 - 生产入库数据列表
 * @param params
 * @returns
 */
export function exportProductInData(
  params: PARAMS_PRODUCTION_IN_DATA,
  fileName: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  return download({
    method: 'post',
    url: '/hebei/drugAdministration/exportAlExecInData',
    params,
    fileConfig: {
      name: fileName,
      type: 'application/zip',
    },
  });
}
