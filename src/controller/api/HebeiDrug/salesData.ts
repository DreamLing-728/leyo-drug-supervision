import { post, download } from '@src/utils/request';
import {
  PARAMS_SALES_DATA,
  RESPONSE_SALES_DATA,
} from '@views/pages/HebeiDrug/salesData/js/type';
/**
 * 陈建容 - 采购数据列表
 * @param params
 * @returns
 */
export function getSalesDataList(
  params: PARAMS_SALES_DATA,
): Promise<RESPONSE_SALES_DATA> {
  return post({
    url: '/hebei/drugAdministration/salesOutDataList',
    params,
  });
}

/**
 * 陈建容 - 导出采购数据列表
 * @param params
 * @returns
 */
export function exportSalesData(
  params: PARAMS_SALES_DATA,
  fileName: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  return download({
    method: 'post',
    url: '/hebei/drugAdministration/exportSalesOutData',
    params,
    fileConfig: {
      name: fileName,
      type: 'application/zip',
    },
  });
}
