import { post, download } from '@src/utils/request';
import {
  PARAMS_DAMAGE_OUT_DATA,
  RESPONSE_DAMAGE_OUT_DATA,
} from '@views/pages/HebeiDrug/damageOut/js/type';
/**
 * 陈建容 - 生产入库数据列表
 * @param params
 * @returns
 */
export function getDamageOutDataList(
  params: PARAMS_DAMAGE_OUT_DATA,
): Promise<RESPONSE_DAMAGE_OUT_DATA> {
  return post({
    url: '/hebei/drugAdministration/alExecOutDataList',
    params,
  });
}

/**
 * 陈建容 - 生产入库数据列表
 * @param params
 * @returns
 */
export function exportDamageOutData(
  params: PARAMS_DAMAGE_OUT_DATA,
  fileName: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  return download({
    method: 'post',
    url: '/hebei/drugAdministration/exportAlExecOutData',
    params,
    fileConfig: {
      name: fileName,
      type: 'application/zip',
    },
  });
}
