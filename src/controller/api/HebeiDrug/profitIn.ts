import { post, download } from '@src/utils/request';
import {
  PARAMS_PROFIT_IN_DATA,
  RESPONSE_PROFIT_IN_DATA,
} from '@views/pages/HebeiDrug/profitIn/js/type';
/**
 * 陈建容 - 采购数据列表
 * @param params
 * @returns
 */
export function getProfitInDataList(
  params: PARAMS_PROFIT_IN_DATA,
): Promise<RESPONSE_PROFIT_IN_DATA> {
  return post({
    url: '/hebei/drugAdministration/invntryExecInDataList',
    params,
  });
}

/**
 * 陈建容 - 导出采购数据列表
 * @param params
 * @returns
 */
export function exportProfitInData(
  params: PARAMS_PROFIT_IN_DATA,
  fileName: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  return download({
    method: 'post',
    url: '/hebei/drugAdministration/exportInvntryExecInData',
    params,
    fileConfig: {
      name: fileName,
      type: 'application/zip',
    },
  });
}
