import { post, download } from '@src/utils/request';
import {
  PARAMS_INVENTORY_DATA,
  RESPONSE_INVENTORY_DATA,
} from '@views/pages/HebeiDrug/inventoryData/js/type';
/**
 * 陈建容 - 库存数据列表
 * @param params
 * @returns
 */
export function getInventoryDataList(
  params: PARAMS_INVENTORY_DATA,
): Promise<RESPONSE_INVENTORY_DATA> {
  return post({
    url: '/hebei/drugAdministration/angleBalanceDataList',
    params,
  });
}

/**
 * 陈建容 - 导出库存数据列表
 * @param params
 * @returns
 */
export function exportInventoryData(
  params: PARAMS_INVENTORY_DATA,
  fileName: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  return download({
    method: 'post',
    url: '/hebei/drugAdministration/exportAngleBalanceData',
    params,
    fileConfig: {
      name: fileName,
      type: 'application/zip',
    },
  });
}
