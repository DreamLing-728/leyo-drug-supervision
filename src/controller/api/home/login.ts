import { post } from '@src/utils/request';
import { PARAMS_LOGIN } from '@src/types/login';
/**
 * 陈建容 - 登录获取token
 * @param params
 * @returns
 */
export function loginApi(params: PARAMS_LOGIN): Promise<string> {
  return post({
    url: '/token/getToken',
    params,
  });
}
