import { MENU_INFO } from '@src/types/layout';

// 根据后台的权限接口获取自身的menu结构树
export const getFilteredMenu = (menuTree: MENU_INFO[], modules: string[]) => {
  const result = [];
  for (const itemAll of menuTree) {
    if (modules.includes(itemAll.module)) {
      // 匹配上一级目录
      const outer = JSON.parse(JSON.stringify(itemAll));
      if (outer.children) outer.children = [];
      if (itemAll.children && itemAll.children.length > 0) {
        // 二级目录
        for (const inner of itemAll.children) {
          if (modules.includes(inner.module)) {
            outer.children?.push(inner);
          }
        }
      }
      result.push(outer);
    }
  }
  return result;
};

export const menu: MENU_INFO[] = [
  {
    label: '待办事项',
    module: 'backlog',
    icon: '#icon-todo-list',
    activeIcon: '#icon-todo-list-active',
    path: '/todo-list',
    id: 1,
  },
  {
    label: '售后工单',
    module: 'after_sale',
    icon: '#icon-all-order',
    activeIcon: '#icon-all-order-active',
    path: '/order-list',
    id: 2,
    children: [
      {
        label: '全部工单',
        module: 'all_work_order',
        path: '/order-list/all-order',
        id: 8,
      },
    ],
  },
];
