// eslint-disable-next-line @typescript-eslint/no-explicit-any
import { NavInstance, MenuItem } from '@src/types/layout';

export declare interface LayoutConfig {
  sideBar: boolean;
  header: boolean;
}

export declare interface AppInfo {
  device: number;
  layoutConfig: LayoutConfig;
  currentPath: string;
  menu: MenuItem[];
  navList: NavInstance[];
  loading: boolean;
  menuCollapse: boolean;
  [prop: string]: any;
}

export interface RootState {
  app: AppInfo;
}
