import { defineStore, createPinia } from 'pinia';

type UserInfo = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [props: string]: any;
};

export const userStore = defineStore('userStore', {
  state: () => {
    return {
      infos: {}, // 用户信息
    };
  },
  actions: {
    setUserInfo(data?: UserInfo) {
      this.infos = data;
    },
  },
});

export const userStoreWithoutSetup = () => {
  return userStore(createPinia());
};
