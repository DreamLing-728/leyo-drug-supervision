import { defineStore, createPinia } from 'pinia';
import { NavInstance, MenuItem } from '@src/types/layout';
import { AppInfo } from '../types/states';
import { DEVICE_TYPE } from '@src/types/common';
import { RouteRecord } from 'vue-router';
import Router from '../../router/index';
import { NOT_SHOW_NAV_MENU } from '../config/config';

const navList: NavInstance[] = [];
const menuList: MenuItem[] = [];

export const appStore = defineStore('appStore', {
  state: (): AppInfo => {
    return {
      layoutConfig: {
        sideBar: true,
        header: true,
      },
      device: 1,
      currentPath: '/',
      menu: menuList,
      navList,
      loading: false,
      menuCollapse: false, // 左侧菜单是否隐藏
    };
  },
  actions: {
    // 设置客户端类型 1--pc端 2--wap端
    setDevice(val: DEVICE_TYPE) {
      this.device = val;
    },
    collapseMenu(val?: boolean) {
      val = !this.menuCollapse;
      this.menuCollapse = val;
    },
    // app loading
    setLoading(val = false) {
      this.loading = val;
    },
    // 设置侧边菜单栏
    setMenu(menu: MenuItem[]) {
      this.menu = menu;
    },
    // 设置顶部栏是否显示
    setHeaderVisible(payload = false) {
      this.layoutConfig.header = payload;
    },
    // 设置侧边栏是否显示
    setSidebaerVisible(payload = false) {
      this.layoutConfig.sideBar = payload;
    },
    // 更新当前路由
    setCurrentPath(payload = '') {
      this.currentPath = payload;
      for (const item of this.navList) {
        if (item.path === payload) {
          item.active = true;
        } else {
          item.active = false;
        }
      }
    },
    // 添加顶部导航栏
    addNavigator(navigatorItem: NavInstance) {
      const path = navigatorItem.path;
      if (NOT_SHOW_NAV_MENU.includes(path)) {
        return;
      }
      if (path) {
        const navigatorList: NavInstance[] = this.navList;
        const targetIndex: number = navigatorList.findIndex(
          (v: NavInstance) => v.path === path,
        );
        if (targetIndex == -1) {
          navigatorList.push(navigatorItem);
        } else {
          navigatorList.splice(targetIndex, 1, navigatorItem);
        }
        // 处理首页逻辑
        const staticIndex: number = navigatorList.findIndex(
          (v: NavInstance) => v.path === '/',
        );
        let indexRoute: NavInstance | undefined = navigatorList[staticIndex];
        if (indexRoute === undefined) {
          const sourceIndexRoute: RouteRecord | undefined =
            Router.getRoutes().find((v: RouteRecord) => v.path === '/');
          if (sourceIndexRoute) {
            indexRoute = {
              path: sourceIndexRoute.path,
              label: sourceIndexRoute.meta.label as string,
              query: {},
              params: {},
            };
          }
        } else {
          staticIndex > -1 && navigatorList.splice(staticIndex, 1);
          navigatorList.unshift(indexRoute);
        }
      }
    },
    // // 弹出顶部导航栏
    popNavigator({ path = '' } = {}) {
      const index = this.navList.findIndex((v: NavInstance) => v.path === path);
      if (index !== -1) {
        this.navList.splice(index, 1);
        if (this.currentPath === path) {
          // 如果关闭的是当前的路由
          const nextIndex = this.navList.lenght === index ? index - 1 : index;
          const nextNav: NavInstance = this.navList[nextIndex];
          let nextPath = '/';
          let nextQuery = {};
          let nextParams = {};
          if (nextNav && nextNav.path) {
            nextPath = nextNav.path;
            nextQuery = nextNav.query || {};
            nextParams = nextNav.params || {};
          }
          Router.push({
            path: nextPath,
            query: nextQuery,
            params: nextParams,
          });
        }
      }
    },
  },
});

export const appStoreWithoutSetup = () => {
  return appStore(createPinia());
};
