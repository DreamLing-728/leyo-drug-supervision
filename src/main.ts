//解决样式被覆盖问题
import { createApp } from 'vue';
import App from './App.vue';
import { createPinia } from 'pinia';
import * as ElementIcons from '@element-plus/icons-vue';
import Router from './controller/router/index';
import microApp from '@micro-zoe/micro-app';
import ElementPlus from 'element-plus';
import zhCn from 'element-plus/es/locale/lang/zh-cn';

import './assets/styles/index.scss';
import 'element-plus/dist/index.css';

const app = createApp(App);

// 引入element-plus的所有icon
for (const iconName in ElementIcons) {
  app.component(
    'i-' + iconName.toLocaleLowerCase(),
    // 规避 indexable 问题
    ElementIcons[iconName as keyof typeof ElementIcons],
  );
}
app.use(ElementPlus, {
  locale: zhCn,
});
app.use(createPinia());
app.use(Router);
app.mount('#app');

microApp.start({});
