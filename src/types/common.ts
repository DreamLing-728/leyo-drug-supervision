export declare interface Obj {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propsName: string]: any;
}

// 设备类型
export const enum DEVICE_TYPE {
  'desktop' = 1, // pc端
  'mobile' = 2, // 手机端
}

export type PaginationParams = {
  current?: number;
  size?: number;
  isOpenPage?: number;
  total?: number;
};

export type Loading = {
  tableLoading?: boolean;
  downLoading?: boolean;
};

// 入库类型
export enum PARAMS_BILL_TYPE {
  采购入库 = 1,
  采购出库 = 2,
}

// 销售类型
export enum PARAMS_SALES_TYPE {
  销售出库 = 1,
  销售退回 = 2,
}

// 品种类型
export enum DRUG_TYPE {
  药品制剂 = 0,
  中药饮片 = 1,
}

// 入库类型-返回
export enum RESPONSE_BILL_TYPE_IN {
  采购入库 = 1,
  采购退回 = 2,
  生产入库 = 3,
  盘盈入库 = 4,
}

// 销售类型-返回
export enum RESPONSE_BILL_TYPE_OUT {
  销售出库 = 1,
  销售退回 = 2,
  抽检出库 = 3,
  报损出库 = 4,
  盘亏出库 = 5,
}
