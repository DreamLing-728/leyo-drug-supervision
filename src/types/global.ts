export type Obj = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propsName: string]: any;
};
