import { RouteRecordName, LocationQuery, RouteParams } from 'vue-router';
export declare interface NavInstance {
  label: RouteRecordName | null | undefined;
  path: string;
  active?: boolean;
  closable?: boolean;
  query?: LocationQuery;
  params?: RouteParams;
}

export declare interface ScrollObject {
  scrollLeft?: number;
  scrollTop?: number;
}

export declare interface MenuItem {
  icon?: string;
  path?: string;
  label: string;
  children?: MenuItem[];
}

export interface MENU_INFO {
  icon?: string;
  activeIcon?: string;
  label: string;
  path: string;
  id: number;
  module: string;
  children?: MENU_INFO[];
}
