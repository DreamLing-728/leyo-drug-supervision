/* eslint-disable prettier/prettier */
import { Obj } from '@src/types/common';
import router from '@src/controller/router';

/**
 * @returns boolean
 */
export function isMobile(): boolean {
  const reg =
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i;
  const flag: boolean = reg.test(navigator.userAgent);
  return flag;
}
/**
 *
 * @param url 需要解析的url
 * @returns 键值对形式的queryString
 */
export function getParams(url: string): Obj {
  const urlArr = url.split('?');
  const queryString: string | undefined = urlArr[1] || urlArr[0];
  const res: Obj = {};
  if (queryString) {
    const queryParams: string[] = queryString.split('&');
    for (const item of queryParams) {
      const keyIndex: number = item.indexOf('=');
      const key: string = item.substring(0, keyIndex);
      const value: string = item.substring(keyIndex + 1);
      res[key] = value;
    }
  }
  return res;
}


// 返回登录页
export function gotoLogin() {
  console.log('返回登录页', router);
  router.replace({
    name: 'login',
  });
}