/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  AxiosInstance,
  AxiosResponse,
  AxiosRequestConfig,
  Method,
} from 'axios';
import { ElMessage } from 'element-plus';
import { getToken } from '../auth';
import { RequestInstance, SERVERCODE, promiseCallback } from './config';
import { gotoLogin } from '@src/utils';

export function setInterceptor(service: AxiosInstance) {
  service.interceptors.request.use(
    (config: any) => {
      const token = getToken();
      config.headers['Content-Type'] = 'application/json';
      config.headers['token'] = token || '';
      if (config.method === 'upload') {
        config.method = 'post';
        config.headers['Content-Type'] = 'multipart/form-data;charset=UTF-8';
      }
      return config;
    },
    (error: any): void => {
      Promise.reject(error);
    },
  );

  service.interceptors.response.use(
    (response: any) => {
      return response;
    },
    (error: any): any => {
      if (
        error.code === 'ECONNABORTED' &&
        error.message.indexOf('timeout') !== -1
      ) {
        errorToast('请求超时！');
      }
      return error;
    },
  );
}

export function generateOptions(
  options: RequestInstance,
  method: Method | 'upload' | 'download',
): AxiosRequestConfig {
  if (!options.params) {
    options.params = {};
  }
  method = method || 'post';
  // 请求头
  const axiosOptions: AxiosRequestConfig = {
    method: options.method,
    url: options.url,
    responseType: options.responseType || 'json',
  };
  switch (method) {
    case 'get':
      axiosOptions.params = options.params;
      break;
    case 'post':
      axiosOptions.data = options.params;
      break;
    case 'upload': {
      const form = new FormData();
      for (const [key, value] of Object.entries(options.params)) {
        form.append(key, value);
      }
      axiosOptions.data = form;
      break;
    }
    // 在download中需要再对get/post进行判断处理
    case 'download': {
      if (options.method === 'get') {
        axiosOptions.params = options.params;
      }
      if (options.method === 'post') {
        axiosOptions.data = options.params;
      }
      break;
    }
  }
  return axiosOptions;
}

export function errorToast(error: string): void {
  ElMessage({
    message: error,
    type: 'error',
  });
}

export function responseHandler(
  response: AxiosResponse,
  resolve: promiseCallback,
  reject: promiseCallback,
): void {
  const { data, status } = response;
  if (status === 200) {
    // resolve(data)
    if (response.request.responseType !== 'blob') {
      const code: number | string = data.code;
      switch (Number(code)) {
        case SERVERCODE.SUCCESS: {
          resolve(data.data || null);
          break;
        }
        case SERVERCODE.ACCESSDENIED:
        case SERVERCODE.EXPIRED:
          errorToast('无权限或token已经过期');
          gotoLogin();
          break;
        case SERVERCODE.UPGRADE: {
          reject(data);
          errorToast(data.message || 'error');
          setTimeout(() => {
            window.location.reload();
          }, 2000);
          break;
        }
        case SERVERCODE.CHANGEPASSWORD: {
          // change password
          break;
        }
        default: {
          if (data.message) {
            errorToast(data.message);
            reject(data);
          }
          break;
        }
      }
    } else {
      resolve(response);
    }
  } else if (status === 404) {
    errorToast('404 not found');
    reject(data);
  } else {
    errorToast(data.message || 'error');
    reject(data);
  }
}
