/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosRequestConfig, Method } from 'axios';

import { fileConfig, RequestInstance } from './config';
import { setInterceptor, generateOptions, responseHandler } from './handler';

const service = axios.create({
  baseURL: '/kernel-api/erp',
  timeout: 300000,
});
setInterceptor(service);

function request(
  options: RequestInstance,
  method: Method | 'upload' | 'download',
): Promise<any> {
  const axiosOptions: AxiosRequestConfig = generateOptions(options, method);
  return new Promise((resolve, reject) => {
    service
      .request(axiosOptions)
      .then((response) => {
        responseHandler(response, resolve, reject);
      })
      .catch((err) => {
        reject(err.response);
      });
  });
}

const post = function (options: RequestInstance): Promise<any> {
  options.method = 'post';
  return request(options, 'post');
};

const get = function (options: RequestInstance): Promise<any> {
  options.method = 'get';
  return request(options, 'get');
};

const upload = function (options: RequestInstance): Promise<any> {
  options.method = 'upload';
  return request(options, 'upload');
};

const download = async function (options: RequestInstance): Promise<any> {
  options.method = options.method || 'post';

  if (!options.responseType) {
    options.responseType = 'blob';
  }
  if (!options.fileConfig) {
    options.fileConfig = {
      type: '',
      name: '',
    };
  }
  try {
    const rsp = await request(options, 'download');
    const { data } = rsp;
    const { name = 'download' } = options.fileConfig as fileConfig;
    // if (data.type && !options.fileConfig.type) {
    //   options.fileConfig.type = data.type;
    // }
    const blob = new Blob([data], { type: options.fileConfig.type });
    const link = document.createElement('a');
    const url = window.URL.createObjectURL(blob);
    link.href = url;
    link.download = name;
    // document.body.appendChild(link);
    link.click();
    // document.body.removeChild(link);
    window.URL.revokeObjectURL(url);
    return rsp;
  } catch (err) {
    return err;
  }
};

export default request;

export { post, get, download, upload };
