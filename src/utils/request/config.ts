export const enum SERVERCODE {
  SUCCESS = 40001, // 成功
  ACCESSDENIED = 40005, // 无权限
  EXPIRED = 40007, // token已经过期
  CHANGEPASSWORD = 40008, // 修改密码
  UPGRADE = 400013, // 强制更新
}
export declare interface AxiosOptions {
  method: string;
  url: string;
  responseType: string;
  params?: object;
  data?: object;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export declare type promiseCallback = (value: any) => any;

export declare interface fileConfig {
  type?: string;
  name?: string;
}

export declare interface RequestInstance {
  params?: object;
  url: string;
  fileConfig?: fileConfig;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propName: string]: any;
}
