import Cookies from 'js-cookie';

export function setToken(token: string) {
  sessionStorage.setItem('squirtle_token', token);

  Cookies.set('squirtle_token', token);
}

export function getToken() {
  return (
    sessionStorage.getItem('squirtle_token') || Cookies.get('squirtle_token')
  );
}

export function clearToken() {
  sessionStorage.removeItem('squirtle_token');

  Cookies.remove('squirtle_token');
}
