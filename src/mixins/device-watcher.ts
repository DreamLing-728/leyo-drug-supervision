import { onBeforeMount, onBeforeUnmount } from 'vue';
import { isMobile } from '@src/utils';
import { appStoreWithoutSetup } from '@store/app';
const app = appStoreWithoutSetup();

export default function Resize(): void {
  const resizeHandler = () => {
    const mobile: boolean = isMobile();
    const device: number = mobile ? 2 : 1;
    app.setDevice(device);
  };
  onBeforeMount(() => {
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  });
  onBeforeUnmount(() => {
    window.removeEventListener('resize', resizeHandler);
  });
}
