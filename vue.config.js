/* eslint-disable no-undef */

/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const { defineConfig } = require('@vue/cli-service');
const AutoImport = require('unplugin-auto-import/webpack');
const Components = require('unplugin-vue-components/webpack');
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers');

// 整体webpack配置
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    open: true,
    port: 9010,
    proxy: {
      '/kernel-api/erp': {
        // target: `http://192.168.27.57:18889`, // 测试服
        target: `http://192.168.208.4:18889`, // 正式服
        changeOrigin: true,
      },
    },
  },
  css: {},
  configureWebpack: {
    plugins: [
      AutoImport({
        imports: ['vue', 'vue-router'],
        dts: './auto-imports.d.ts',
        eslintrc: {
          //解决eslint报错
          enabled: true,
          filepath: './.eslintrc-auto-import.json',
          globalsPropValue: true,
        },
        resolvers: [ElementPlusResolver({ importStyle: false })],
      }),
      Components({
        resolvers: [ElementPlusResolver({ importStyle: false })],
      }),
    ],
  },
  chainWebpack: (config) => {
    config.resolve.alias.set('@src', resolve('src'));
    config.resolve.alias.set('@api', resolve('src/controller/api'));
    config.resolve.alias.set('@store', resolve('src/controller/store/modules'));
    config.resolve.alias.set('@views', resolve('src/views'));
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap((options) => {
        options.compilerOptions = {
          ...(options.compilerOptions || {}),
          isCustomElement: (tag) => /^micro-app/.test(tag),
        };
        return options;
      });
    // svg处理
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule.include
      .add(resolve('src/assets/svgs'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icarus-[name]',
      })
      .end();
  },
});

// 文件路径

function resolve(dir) {
  return path.join(__dirname, dir);
}
